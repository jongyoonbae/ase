from abc import ABC, abstractmethod
import time

import numpy as np

from ase.neb import idpp_interpolate, BaseNEB, interpolate
from ase.optimize import QuasiNewton
from ase.optimize.optimize import Optimizer
from ase.geometry import find_mic
from ase.constraints import FixedMode, FixInternals, FixBondLengths
from ase.utils import lazyproperty
from ase.utils.forcecurve import fit_images
from ase.io import write


class CFIState:
    # FIXME/ap: Should this be a namedtuple like ForceFit in
    # ase.utils.forcecurve?
    """
    Stores the current state of CFI. Specifically, all the images and their
    energies, as well as the index of the lasted interpolated image and its
    constraint-force.
    """

    def __init__(self, images, energies):
        self.images = images
        self.energies = energies
        self.i_last = None
        self.cf_last = np.nan
        self.interpolated_images = []

    @lazyproperty
    def eref(self):
        return self.images[0].get_potential_energy()

    @property
    def imax(self):
        return np.argsort(self.energies)[-1]


class CFIMethod(ABC):
    """
    Base class containing the core CFI methods. It includes identifying a pair
    of bound images, followed by an constraint-force-based interpolation of
    a new image in between. The interpolated image is optimized and added to
    the state. Constraint-specific methods are defined in derived classes.
    """

    def __init__(self, cfi, state, indices):
        self.cfi = cfi
        self.indices = indices
        self.initial = state.images[0]
        self.final = state.images[-1]

    @lazyproperty
    def constraints(self):
        return self.initial.constraints.copy()

    @abstractmethod
    def get_constraint_vector(self, state, i):
        """
        Returns a normalized vector in the direction of constraint.
        """
        ...

    @abstractmethod
    def get_constraint_force(self, state, i):
        """
        Returns a scalar force in the direction of the constraint-vector.
        That is, the forces on constrained atoms projected onto
        the constraint-vector.
        """
        ...

    @abstractmethod
    def get_constraint_coordinate(self, *args):
        """
        Returns a value of the constrained parameter.
        """
        ...

    @abstractmethod
    def guess_bound_states(self, state, i, cf):
        """
        Based on the initial state, final state, and a constraint-force,
        guess a pair of bound states.
        """
        ...

    @abstractmethod
    def set_and_apply_constraint(self):
        ...

    def get_bound_states(self, state, i):
        """
        Finds a pair of images with oppositely signed constraint-forces.
        One of the images is i, and the other is either i-1 or i+1, depending
        upon the sign.
        Returns the indices of the two images along with their forces along
        the constraint mode.
        """
        if self.cfi.has_two_images:
            # When there are only two images, they become bounds
            return (0, 1, self.get_constraint_force(state, 0),
                    self.get_constraint_force(state, 1))

        # FIXME/ap: My intuition is we should be calculating 2 constraint
        # forces for image i: one with the image to the left, and one with
        # the image to the right. Then we compare the right-facing one with
        # the image to the right, and the left-facing one with the image to
        # the left.
        # Or else we just use the centered finite-difference approach for
        # all three, which is a bit simpler and maybe avoids an of the inconsistent
        # force here. Although ultimately when we interpolate, we should be using
        # the top method.
        # Which we use probably will only subtly change things, but if we choose
        # wrong there could be weird bugs that are hard to track down, and/or
        # inconsistencies if a pathway is reversed.
        # But properly we are looking for a sign change between a pair of images
        # with this method, so properly the first method must be the right one.
        # In principle we can get multiple sign changes no matter what we do.
        cf_backward = self.get_constraint_force(state, i, 'backward')
        cf_forward = self.get_constraint_force(state, i, 'forward')
        cf_left = self.get_constraint_force(state, i - 1, 'forward')
        cf_right = self.get_constraint_force(state, i + 1, 'backward')
        # Actually, let's posit that image i, which we are forcing to be in the
        # new pair, was created by interpolating between images i-1, i+1, which
        # should be the normal case. Then we know that i-1 has a constraint
        # force that is negative and i+1 has a positive one. So it would in
        # principle be enough to check the sign of the new image i, in a
        # centered mode, and if it's negative then (i, i+1) is correct, and if
        # it's positive then (i-1, i) is correct. If we want to keep things
        # simple.

        fmax = self.cfi.fmax
        if abs(cf_left) < fmax or abs(cf_right) < fmax:
            # When either of the bounds has a zero force (below fmax), guess
            cf = self.get_constraint_force(state, i, 'central')
            return self.guess_bound_states(state, i, cf)

        # Prediction of a TS structure where constraint-force is zero
        # can only be interpolated with a pair of images with oppositely
        # signed constraint-forces
        result = []
        if cf_backward > 0 and cf_left < 0:
            result.append('left')
        if cf_forward < 0 and cf_right > 0:
            result.append('right')
        if len(result) == 0:
            raise RuntimeError('Did not find bound images!')
        if len(result) == 2:
            raise NotImplementedError('Found a sign change on both sides of '
                                      'the new image. Unsure which to choose!')
        if result[0] == 'left':
            return i - 1, i, cf_left, cf_backward
        if result[0] == 'right':
            return i, i + 1, cf_forward, cf_right

    def get_linear_root(self, cfs, crs):
        if cfs[0] > cfs[-1]:
            cfs.reverse()
            crs.reverse()
        # Calculate a constraint-coordinate where the constraint-force
        # is zero through a linear interpolation
        cr = np.interp(0.0, cfs, crs)
        return cr

    def get_cubic_coeffs(self, energies, cfs, crs):
        e1, e2 = energies
        cf1, cf2 = cfs
        cr1, cr2 = crs

        # Solve for a cubic polynomial using both energy and force
        c = np.linalg.solve(np.array([(1, cr1, cr1**2, cr1**3),
                                      (1, cr2, cr2**2, cr2**3),
                                      (0, 1, 2 * cr1, 3 * cr1**2),
                                      (0, 1, 2 * cr2, 3 * cr2**2)]),
                            np.array([e1, e2, -cf1, -cf2]))
        return c

    def get_quad_root(self, c, cr1, cr2):
        # Calculate two constraint-coordinates with
        # a zero constraint-force
        x1, x2 = np.roots([-3 * c[3], -2 * c[2], -c[1]])
        # Pick one that falls in between the bounds
        if cr1 < x1 < cr2 or cr1 > x1 > cr2:
            return x1
        elif cr1 < x2 < cr2 or cr1 > x2 > cr2:
            return x2
        else:
            raise RuntimeError('Did not find a valid quadratic root')

    def get_interpolation_extent(self, image1, image2, cf1, cf2):
        """
        Calculates an extent of interpolation where 0.0 means image1,
        0.5 means the midpoint, and 1.0 means image2. Returns the interpolation
        extent and interpolated constraint-coordinate.
        """
        if isinstance(self, FixedModeMethod):
            cr1 = 0.0
            cr2 = self.get_constraint_coordinate(image1, image2)
        else:
            cr1 = self.get_constraint_coordinate(image1)
            cr2 = self.get_constraint_coordinate(image2)

        fmax = self.cfi.fmax
        if abs(cf1) < fmax or abs(cf2) < fmax:
            # When either of the bounds has a zero force, make a midpoint
            # interpolation
            ext = 0.5
            cr_int = (cr1 + cr2) * 0.5
        else:
            if np.sign(cf1) == np.sign(cf2):
                raise RuntimeError('Two constraint-forces have the same sign')

            int_method = self.cfi.interpolate_method
            cfs = [cf1, cf2]
            crs = [cr1, cr2]
            if int_method == 'linear':
                cr_int = self.get_linear_root(cfs, crs)
            elif int_method == 'quadratic':
                Es = [image1.get_potential_energy(),
                      image2.get_potential_energy()]
                c = self.get_cubic_coeffs(Es, cfs, crs)
                cr_int = self.get_quad_root(c, cr1, cr2)
            else:
                raise NotImplementedError(
                    f'Interpolation with {int_method} '
                    'is not implemented')
            ext = (cr_int - cr1) / (cr2 - cr1)
        return ext, cr_int

    def get_interpolate_image(self, state, i1, i2, cf1, cf2):
        """
        Create an interpolated image and apply constraints. Returns
        interpolated image and its index.
        """
        image1 = state.images[i1]
        image2 = state.images[i2]
        ext, cr_int = self.get_interpolation_extent(
            image1, image2, cf1, cf2)
        img_int = image1.copy()
        img_int.set_calculator(self.cfi.calculator)

        # Interpolate all the atomic positions
        if ext == 0.5:
            images = [image1, img_int, image2]
            # For a midpoint interpolation, use idpp scheme
            # FIXME/ap: JY used idpp_interpolate; can we make this optional?
            # I'm just temporarily turning it off.
            idpp = False
            if idpp:
                idpp_interpolate(images, traj=None, log=None)
            else:
                interpolate(images)
        else:
            # FIXME/ap: JY had as using the images pbc; I don't think
            # we want this as in a reaction nothing wraps around?
            # I am trying without.
            # d, _ = find_mic(image2.positions - image1.positions,
            #                 image1.cell, image1.pbc)
            d, _ = find_mic(image2.positions - image1.positions,
                            image1.cell, pbc=False)
            # Interpolate based on calculated extent and direction d.
            img_int.positions += d * ext

        # Adjust the constraint-coordinate and apply constraint
        # FIXME/ap: a bit weird to have two different call signatures,
        # maybe we can find a better way, especially since these have ABCs.
        if isinstance(self, FixedModeMethod):
            self.set_and_apply_constraint(img_int, state, i1)
        else:
            self.set_and_apply_constraint(img_int, cr_int)
        return img_int, i1 + 1

    def optimize_and_add_image(self, state, show_intermediate=False):
        print('='*40)
        print('='*40)
        print('='*40)
        print('in optimize_and_add_image')
        optimizer = self.cfi.optimizer
        fmax = self.cfi.fmax
        nsteps = self.cfi.nsteps
        print(f'nsteps: {nsteps}')

        i1, i2, cf1, cf2 = self.get_bound_states(state, state.i_last)
        print('bound states:')
        print(i1, i2, cf1, cf2)
        img_int, i_int = self.get_interpolate_image(state, i1, i2, cf1, cf2)

        # Optimize the interpolated image
        if show_intermediate:
            opt = optimizer(img_int, trajectory='int{}.traj'.format(nsteps),
                            logfile='int{}.log'.format(nsteps))
        else:
            opt = optimizer(img_int, trajectory=None, logfile=None)
        opt.run(fmax=fmax)
        if hasattr(opt, 'force_calls'):
            self.cfi.force_calls += opt.force_calls
        else:
            self.cfi.force_calls += opt.nsteps
        # FIXME/ap: Above is skipping a step per interior image.
        # I think it is anyway. E.g., it numbers steps 0-7 for 8 steps
        # but I think it would only add on 7 in that case, not 8.
        del img_int.constraints[-1]

        # Add to the state
        state.images.insert(i_int, img_int)
        # Append to the list of calculated images
        state.interpolated_images.append(img_int)
        if self.cfi.has_two_images:
            self.cfi.has_two_images = False
        # Update the latest constraint-force and index
        state.cf_last = self.get_constraint_force(state, i_int)
        state.i_last = i_int
        # Log the progess
        self.cfi.log(img_int.get_potential_energy(), state.cf_last)
        # XXX/ap: Why does this work? Shouldn't we have frozen results
        # before sticking into the state? I guess it doesn't matter
        # since it's the same object?
        BaseNEB.freeze_results_on_image(
            img_int, energy=img_int.get_potential_energy(),
            forces=img_int.get_forces())


class FixedBondMethod(CFIMethod):
    """
    Assumes a bond length between two atoms as a reaction cooridnate
    and therefore the bond length is the constraint-coordinate.
    """

    @lazyproperty
    def is_increasing(self):
        cr_ini = self.get_constraint_coordinate(self.initial)
        cr_fin = self.get_constraint_coordinate(self.final)
        return cr_ini < cr_fin

    def get_constraint_vector(self, state, i):
        image = state.images[i]
        cv = (image.positions[self.indices[1]]
              - image.positions[self.indices[0]])
        cv /= np.linalg.norm(cv)
        return cv, -cv

    def get_constraint_force(self, state, i):
        cv1, cv2 = self.get_constraint_vector(state, i)
        forces = state.images[i].get_forces()
        cf = (np.vdot(forces[self.indices[0]], cv1)
              + np.vdot(forces[self.indices[1]], cv2))
        return cf

    def get_constraint_coordinate(self, image):
        return image.get_distance(*self.indices)

    def guess_bound_states(self, state, i, cf):
        if i == 0:
            return i, i + 1, cf, self.get_constraint_force(state, i + 1)
        elif i == len(state.images) - 1:
            return i - 1, i, self.get_constraint_force(state, i - 1), cf

        if self.is_increasing and cf > 0:
            return i, i + 1, cf, self.get_constraint_force(state, i + 1)
        elif self.is_increasing and cf < 0:
            return i - 1, i, self.get_constraint_force(state, i - 1), cf
        elif not self.is_increasing and cf > 0:
            return i - 1, i, self.get_constraint_force(state, i - 1), cf
        else:
            return i, i + 1, cf, self.get_constraint_force(state, i + 1)

    def set_and_apply_constraint(self, img_int, cr_int):
        img_int.set_distance(*self.indices, cr_int)
        c = self.constraints
        c.append(FixBondLengths([[*self.indices]]))
        img_int.set_constraint(c)


class FixedDihedral(FixedBondMethod):
    """
    Assumes a dihedral angle among four atoms as a reaction cooridnate
    and therefore the dihedral angle is the constraint-coordinate.
    """

    def get_constraint_vector(self, state, i):
        image = state.images[i]
        a1, a2, a3, a4 = self.indices
        v1 = image.positions[a2] - image.positions[a1]
        v2 = image.positions[a3] - image.positions[a2]
        v3 = image.positions[a4] - image.positions[a3]
        cv1 = np.cross(v2, v1)
        cv1 /= np.linalg.norm(cv1)
        cv2 = np.cross(v3, v2)
        cv2 /= -np.linalg.norm(cv2)
        return cv1, cv2

    def get_constraint_force(self, state, i):
        cv1, cv2 = self.get_constraint_vector(state, i)
        forces = state.images[i].get_forces()
        cf = (np.vdot(forces[self.indices[0]], cv1)
              + np.vdot(forces[self.indices[1]], cv1)
              + np.vdot(forces[self.indices[2]], cv1))
        cf += (np.vdot(forces[self.indices[1]], cv2)
               + np.vdot(forces[self.indices[2]], cv2)
               + np.vdot(forces[self.indices[3]], cv2))
        return cf

    def get_constraint_coordinate(self, image):
        return image.get_dihedral(*self.indices)

    def set_and_apply_constraint(self, img_int, cr_int):
        img_int.set_dihedral(*self.indices, cr_int)
        c = self.constraints
        dihedral = [cr_int, self.indices]
        c.append(FixInternals(dihedrals_deg=[dihedral]))
        img_int.set_constraint(c)


class FixedModeMethod(CFIMethod):
    """
    Assumes the mode among all atoms as a reaction coordinate
    and therefore the mode is the constraint-coordinate. The mode
    is calculated as a vector toward a next image.
    """
    # FIXME/ap Would it be better to use a centered finite-difference
    # rather than a forward finite difference? That is, calculate the
    # mode as the vector between the next and previous image? Since
    # there is not such a thing as directionality (IS and FS are arbitrary)
    # this would seem to make the method less biased. E.g., now it would
    # seem you would get different behavior if you fed [image1, image2]
    # versus [image2, image1]?
    """
    The method can be 'central', 'forward', or 'backward': central
    gives the finite difference between images i-1 and i+1, forward does
    i to i+1, and backward does i-1 to i.
    If an edge image is supplied only forward or backward is possible.
    """

    def get_constraint_vector(self, state, i, method='forward'):
        if i == 0:
            image1 = state.images[0]
            image2 = state.images[1]
        elif i == len(state.images) - 1:
            image1 = state.images[i - 1]
            image2 = state.images[i]
        else:
            if method == 'forward':
                image1 = state.images[i]
                image2 = state.images[i + 1]
            elif method == 'backward':
                image1 = state.images[i - 1]
                image2 = state.images[i]
            elif method == 'central':
                image1 = state.images[i - 1]
                image2 = state.images[i]
            else:
                raise RuntimeError('Bad interpolative method specified.')
        # FIXME/ap: JY had as using the images pbc; I don't think
        # we want this as in a NEB nothing wraps around?
        # I am trying without.
        # cv, _ = find_mic(image2.positions - image1.positions,
        #                  image1.cell, image1.pbc)
        cv, _ = find_mic(image2.positions - image1.positions,
                         image1.cell, pbc=False)
        cv /= np.linalg.norm(cv)
        print('constraint vector')
        print(cv)
        return cv

    def get_constraint_force(self, state, i, method='forward'):
        cv = self.get_constraint_vector(state, i)
        forces = state.images[i].get_forces()
        cf = np.vdot(forces, cv)
        return cf

    def get_constraint_coordinate(self, image1, image2):
        cv, _ = find_mic(image2.positions - image1.positions,
                         image1.cell, image1.pbc)
        cv_norm = cv / np.linalg.norm(cv)
        cr = np.vdot(cv, cv_norm)
        return cr

    def guess_bound_states(self, state, i, cf):
        if i == 0:
            left_image = 0
            right_image = 1
        elif i == len(state.images) - 1:
            left_image = i - 1
            right_image = i
        elif cf < 0:
            left_image = i
            right_image = i + 1
        else:
            left_image = i - 1
            right_image = i
        cf_left = self.get_constraint_force(state, left_image, 'forward')
        cf_right = self.get_constraint_force(state, right_image, 'backward')
        return left_image, right_image, cf_left, cf_right

    def set_and_apply_constraint(self, img_int, state, i):
        # XXX/ap Regardless of the FD method (forward, cetner) this
        # gives the same constraint vector, since i1, img_int, i2 are
        # all along the same vector. So everything is relaxed fine.
        # BTW, we already have this vector calculated by the time this
        # method is called; since it has a custom call signature we
        # could also just feed in the vector directly, in the case of
        # fixed mode.
        c = self.constraints
        cv = self.get_constraint_vector(state, i)
        c.append(FixedMode(cv))
        img_int.set_constraint(c)


def get_cfi_method(cfi, state, indices):
    if indices is None:
        cfi.constraint = 'fixed-mode'
        return FixedModeMethod(cfi, state, indices)
    elif len(indices) == 2:
        cfi.constraint = 'fixed-bond'
        return FixedBondMethod(cfi, state, indices)
    elif len(indices) == 4:
        cfi.constraint = 'fixed-dihedral'
        return FixedDihedral(cfi, state, indices)
    else:
        raise NotImplementedError(
            f'CFI with {len(indices)} indices is not implemented')


class CFI(Optimizer):
    """
    Constraint-force interpolative (CFI) method. CFI finds a transition
    state by maximizing in the direction of constrained coordinate and
    minimizing in all the other degrees of freedom. Naturally, it assumes the
    constrained coordinate as a reaction coordinate. It is designed to
    efficiently reach toward a transition state by predicting its structure
    through force-based interpolations, specifically, by using
    constraint-forces. Constraint-force is a scalar force calculated by
    projecting forces onto a vector corresponding to the constraint.

    J. Bae, C. F. Goldsmith, and A. A. Peterson, In preparation, (2023)

    Examples of CFI are shown in:

    J. Bae, J. Hashemi, D. Yun, D. Kim, D. Choo, C. F. Goldsmith, and
    A. A. Peterson, Catal. Sci. Technol. 12, 6903 (2022).
    :doi:`10.1039/d2cy00907b`
    """

    def __init__(self, images, calculator, indices=None,
                 optimizer=QuasiNewton, interpolate_method='linear',
                 logfile='cfi.log', trajectory='cfi.traj'):
        # FIXME/ap: Would it be less confusing to have a method keyword?
        # It could be confusing to have the indices refer to "constrained" atoms
        # when actually they are the ones that are moving.
        """
        images: list of Atoms objects
            Images may contain only initial and final states or any number
            of additional intermediate states.
        calculator: Calculator object
            Calculator to be attached to interpolated images.
        indices: list of atomic indices
            Indices of constrained atoms. Depending on the length of indices,
            a constraint-specific method is assigned.
            * 2 indices: fixed-bond method
            * 4 indices: fixed-dihedral method
            * None: fixed-mode method
            Note: This specifies atoms to be moved in the transition state.
            Use traditional ASE constraints (like FixAtoms) to fix atoms.
        optimizer: Optimizer object
            Optimizer for interpolated images optimizations.
        interpolate_method: string of method
            Interpolation method to predict a transition state constrained
            parameter. Two methods are available:
            * linear: linearly interpolate using force information
            * quadratic: interpolate with a quadratic model fitted using
            both force and energy information
        logfile: string
            Name of the logfile. Use '-' for stdout.
        trajectory: string
            Name of the trajectory file. Use *None* for no trajectory.
        """
        self.calculator = calculator
        self.optimizer = optimizer
        self.interpolate_method = interpolate_method
        self.nsteps = 0
        self.force_calls = 0
        if logfile is None:
            self.logfile = None
        else:
            self.logfile = self.openfile(file=logfile, mode='a')
        self.trajectory = trajectory
        self.has_two_images = len(images) == 2

        if len(images) < 2:
            raise ValueError('Minimum two images are required')

        for img in images:
            if len(img) != len(images[0]):
                raise ValueError('Images have different numbers of atoms')
            if np.any(img.pbc != images[0].pbc):
                raise ValueError('Images have different boundary conditions')
            if np.any(img.get_atomic_numbers() !=
                      images[0].get_atomic_numbers()):
                raise ValueError('Images have atoms in different orders')

        energies = np.empty(len(images))
        for i, img in enumerate(images):
            # FIXME/ap: Can use calc to get energy if this fails?
            # This has happened to me because I changed something
            # about the images; presumably they'll all use the same calc.
            energies[i] = img.get_potential_energy()
        self.state = CFIState(images, energies)
        self.method = get_cfi_method(self, self.state, indices)

    @property
    def is_finished(self):
        i = self.state.i_last
        if i == 0 or i == len(self.state.images) - 1:
            return False
        cf = self.state.cf_last
        return abs(cf) < self.fmax

    def check_iterations(self):
        if self.is_finished:
            return
        self.nsteps += 1
        if self.nsteps > self.max_iters:
            raise CFIMaxIterError('Maximum iterations exceeded')

    def log(self, e, cf):
        if self.logfile is None:
            return
        T = time.localtime()
        e -= self.state.eref
        name = f'{self.__class__.__name__} ' + self.constraint
        if self.nsteps == 0:
            args = (" " * len(name), "Step", "FC", "Time", "Energy", "cf")
            msg = "%s  %4s[%3s] %8s %15s %12s\n" % args
            self.logfile.write(msg)

        args = (name, self.nsteps, self.force_calls,
                T[3], T[4], T[5], e, cf)
        msg = "%s:  %3d[%3d] %02d:%02d:%02d %15.6f %12.4f\n" % args
        self.logfile.write(msg)
        self.logfile.flush()

    def run(self, max_iters=10, fmax=0.05, show_intermediate=True,
            save_cfi_band=True):
        """
        Finds a transition state by iteratively predicting where the force
        along the constraint-coordinate is zero and checking its force.

        max_iters: integer
            Maximum number of CFI predict-and-check iteration.
        fmax: float
            Force convergence criteria. Must be equal to the Optimizer fmax.
        show_intermediate: bool
            Show each interpolated image optimization progress with a logfile
            and trajectory file.
        save_cfi_band: bool
            Save sorted CFI band in the order of initial state, interpolated
            images, and final state.
        """
        self.fmax = fmax
        self.max_iters = max_iters - 1
        if self.has_two_images:
            # If a pair of images are provided, the pair will be the bounds
            self.state.i_last = 0
        else:
            # If more than two images are provided, use the maximum-energy
            # image as a bound
            self.state.i_last = self.state.imax
            self.state.cf_last = self.method.get_constraint_force(
                self.state, self.state.imax)

        if self.is_finished:
            # If provided images fulfills the force convergence criteria,
            # log and save trajectory
            # FIXME/ap: Could this bit of text be safely added to the end
            # (after the while not) and unindent / remove the if?
            # It should only get to it if is_finished = True because
            # of the while not loop.
            self.log(
                self.state.images[self.state.i_last].get_potential_energy(),
                self.state.cf_last)
            if self.trajectory is not None:
                write(self.trajectory, self.state.images[self.state.i_last])
                if save_cfi_band:
                    write('cfi_band.traj', self.state.images)

        while not self.is_finished:
            self.method.optimize_and_add_image(self.state, show_intermediate)

            if self.trajectory is not None:
                write(self.trajectory, self.state.interpolated_images)
                if save_cfi_band:
                    write('cfi_band.traj', self.state.images)
            self.check_iterations()


class CFITools:
    """
    Class to generate a CFI plot.
    """

    def __init__(self, images, indices=None):
        """
        images: list of Atoms objects
            Same as in CFI class.
        indices: list of atomic indices
            Same as in CFI class.
        """
        self.images = images
        self.indices = indices

        energies = np.empty(len(images))
        for i, img in enumerate(images):
            energies[i] = img.get_potential_energy()
        self.state = CFIState(images, energies)
        self.method = get_cfi_method(self, self.state, indices)

    def sort_images(self):
        """
        Sort images based on constraint coordinate. The first image is assumed
        to be the initial state.
        """
        images = self.images.copy()
        energies = self.state.energies.copy()
        n_images = len(images)
        diffs = np.empty(n_images)

        for i in range(n_images):
            if isinstance(self.method, FixedModeMethod):
                diff, _ = find_mic(images[i].positions - images[0].positions,
                                   images[0].cell, images[0].pbc)
                diffs[i] = np.sqrt((diff**2).sum())
            else:
                diffs[i] = self.method.get_constraint_coordinate(images[i])

        diffs -= diffs[0]
        # Sorts in the order of vicinity from the initial state
        # making the final state to be the last image
        for i, j in enumerate(np.argsort(np.abs(diffs))):
            self.images[i] = images[j]
            self.state.energies[i] = energies[j]

    def plot_band(self, fig=None):
        """
        Returns a CFI plot as a Figure object showing energy
        and constraint-force againt constraint-coordinate.

        fig: pyplot Figure object
            When None, creates a new Figure object.
        """
        # TODO/ap: I improved this to show a nebplot-style
        # energy plot; which works for fixed mode. I don't know
        # if it works for fixed bond length and dihedral.
        if fig is None:
            from matplotlib import pyplot
            fig = pyplot.figure(figsize=(5., 8.))

        ax0 = fig.add_subplot(2, 1, 1)
        ax1 = fig.add_subplot(2, 1, 2, sharex=ax0)

        images = self.images
        n_images = len(images)
        es = self.state.energies
        es -= self.state.eref
        cfs = np.empty(n_images)
        crs = np.empty(n_images)
        # FIXME/ap: The es can go away.
        # FIXME/ap: Note its inconsistent that energy plot
        # uses forcefit to get x positions and force plot
        # uses get_constraint_coordinate. These should be
        # the same for fixed mode if central finite difference
        # used.

        forcefit = fit_images(images)
        forcefit.plot(ax=ax0)

        for i in range(n_images):
            cfs[i] = self.method.get_constraint_force(self.state, i)
            if isinstance(self.method, FixedModeMethod):
                if i == 0:
                    crs[i] = 0
                else:
                    cr = self.method.get_constraint_coordinate(
                        images[i], images[i - 1])
                    crs[i] = cr + crs[i - 1]
            else:
                crs[i] = self.method.get_constraint_coordinate(images[i])

        # ax0.plot(crs, es, 'bo-')
        # ax0.set_ylabel('energy [eV]')

        ax0.set_xlabel('')
        # ax0.set_xticklabels([])
        pyplot.setp(ax0.get_xticklabels(), visible=False)
        ax1.axhline(color='k', linewidth=0.5)
        ax1.plot(crs, cfs, 'ro-')
        ax1.set_ylabel(r'constraint force [eV/Å]')
        # ax1.set_xlabel('constraint coordinate')
        ax1.set_xlabel(r'path [Å]')

        return fig


class CFIMaxIterError(Exception):
    pass
