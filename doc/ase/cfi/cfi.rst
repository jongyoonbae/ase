.. _cfi:

=====================================
Constraint-force interpolative method
=====================================

.. module:: ase.cfi

FIXME/ap Theory and citation here.

---------------------------------------
Alternative to climbing image (CFI-NEB)
---------------------------------------

The CFI method can also make an ideal complement to the climbing-image portion of nudged elastic band (CI-:mod:`NEB <ase.neb>`) calculations.
CFI-NEB which often converges to the saddle point faster than CI-NEB, and sometimes converges when CI-NEB has difficulties.
Note that CFI-NEB is not guaranteed to converge, but when it does, it tends to converge quickly.
When it fails to converge, it fails quickly and obviously -- so one could fall back to the traditional CI-NEB in that case.

Here we will show a simple example of the use of CFI-NEB, contrasting it with the traditional CI-NEB.
A common workflow is to first converge a traditional NEB, and after this converges turn on the climbing image.
So let's start by making and converging a simple NEB:

.. literalinclude:: cfineb01-neb.py

After it converges, we have a minimum-energy path without a saddle-point, and thus only an estimate of the barrier:

.. image:: neb.png

Climbing image approach
-----------------------

For CI-NEB, one would typically now employ a climbing image to hone in on the saddle point by setting :code:`climb=True`:

.. literalinclude:: cfineb03-cineb.py

When we run this script, we find it takes 40 band force calls to converge; with four interior images, this is 160 force calls.
The final path looks like below, where a saddle point is shown.
As seen on the plot below, the true barrier energy is a bit higher than what was estimated without the climbing image.

.. image:: ci-neb.png

CFI approach
------------

Instead, we can use the CFI to interpolate between the two top images of the band, and use the constrained-force approach to find the saddle point.

.. literalinclude:: cfineb05-cfineb.py

As seen on the plot below, the barrier it found has identical energy to that found by CI-NEB.
We see extremely fast convergence: it finds the saddle point with only 1 CFI relaxation, consisting of 6 force calls (compared to 160 force calls with the climing image method).
The comparison won't always be this dramatic, but CFI usually can converge in only a few steps.
A good strategy is to start with CFI-NEB, and if it doesn't work, fall back to CI-NEB.

.. image:: cfi-neb.png

If you only need a saddle point (not the full minimum energy path), it is reasonable to attempt the CFI between the top two images before the NEB has fully converged.
You may also want to pair it with the :class:`DyNEB<ase.neb.DyNEB>` approach for further speed improvements.
