# creates:  neb.traj
from ase.build import fcc100, add_adsorbate
from ase.constraints import FixAtoms
from ase.calculators.emt import EMT
from ase.optimize import BFGS
from ase.neb import NEB, interpolate

# Make a simple, assymetric test system.
initial = fcc100('Al', size=(3, 3, 3), vacuum=10.0)
add_adsorbate(initial, 'Au', 1.7, 'hollow')
initial[21].symbol = 'Pt'
initial[19].symbol = 'Cu'
constraint = FixAtoms(mask=[atom.tag > 1 for atom in initial])
initial.set_constraint(constraint)
initial.calc = EMT()

# Relax the initial and final state.
qn = BFGS(initial).run(fmax=0.01)
final = initial.copy()
final.calc = EMT()
final[-1].x += final.get_cell()[0, 0] / 3
qn = BFGS(final).run(fmax=0.01)

# Set up and run the NEB.
images = [initial]
for i in range(4):
    image = initial.copy()
    image.calc = EMT()
    image.set_constraint(constraint)
    images.append(image)
images.append(final)
interpolate(images)
neb = NEB(images)
neb.interpolate()
qn = BFGS(neb, trajectory='neb.traj')
qn.run(fmax=0.01)
