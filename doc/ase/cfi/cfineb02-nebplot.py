# creates:  neb.png
import ase.io
from ase.neb import NEBTools

images = ase.io.read('neb.traj@-6:')
nebtools = NEBTools(images)
fig = nebtools.plot_band()
fig.savefig('neb.png')
