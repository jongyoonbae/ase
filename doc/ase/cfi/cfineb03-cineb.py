# creates:  ci-neb.traj
import ase.io
from ase.calculators.emt import EMT
from ase.optimize import BFGS
from ase.neb import NEB


images = ase.io.read('neb.traj@-6:')
for image in images[1:-1]:
    image.calc = EMT()
neb = NEB(images, climb=True)
qn = BFGS(neb, trajectory='ci-neb.traj')
qn.run(fmax=0.01)
