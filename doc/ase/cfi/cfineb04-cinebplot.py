# creates:  ci-neb.png
import ase.io
from ase.neb import NEBTools

images = ase.io.read('ci-neb.traj@-6:')
nebtools = NEBTools(images)
fig = nebtools.plot_band()
fig.savefig('ci-neb.png')
