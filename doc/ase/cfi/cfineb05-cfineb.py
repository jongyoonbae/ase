# creates:  cfi.traj
import ase.io
from ase.cfi import CFI
from ase.calculators.emt import EMT


images = ase.io.read('neb.traj@-6:')

cfi = CFI(images=images,
          calculator=EMT(),
          )
cfi.run(fmax=0.01)
