# creates:  cfi-neb.png
import ase.io
from ase.neb import NEBTools

images = ase.io.read('cfi_band.traj', index=':')
nebtools = NEBTools(images)
fig = nebtools.plot_band()
fig.savefig('cfi-neb.png')
